
> Author : Visvesh Kasodariya | Date:15-Mar-2021

# Table of content:
- [1. Protocols Study Guide](#protocols)
- [2. Internet Protocol](#IP)
--------------------------------------------------------------------------------
<a name="protocols"/>

## **1. Protocols - Study Guide.**

- In computer network, Machine talks to each other by means of _protocols_.
- Protocols ensure that diff comps and diff hardware & softs can communicate.
- Large variety of protocols available, each with its own purpose.

#### 1.1 **Packets**.

- Goal of networked computers is to exchange info; this information is carried out by _packets_.
- Packets are streams of bits running as _electrical signals_ on physical media (cables/wifi) used of data transmission. Media can be **wire** in LAN or **the air** in WiFi network.
- This electrical signals are interpreted as bits (_zeros and ones_) that make up the information.

##### 1.1.1 **Packet Structure.**

- Every packet in every protocol has the following structure. _Header + Payload_.
- _Header_ : this ensure that the receiving host can interpret the payload and handle the overall communication.
- _payload_ : It is actual information, It could be something like part of an email message or the content of a file during a download.
- IP Protocol header is at least **160 bits (20 bytes) long**.

#### 1.2 **Protocol Layers**

- There are many protocols, each one for a specific purposes:
	- Exchanging emails client, files(FTP), browsing.. etc
	- Establishing a communication between a server and client
	- Identify hosts on a network
	- Use the physical media to send packets.
- Above List of examples can be rewritten in terms of IP Layers.
	- **Application Layer**
	- **Transport Layer**
	- **Network Layer**
	- **Physical Layer**
- Layers work on top of one another, Every layer has it's own __protocol__ (_**A network protocol is an established set of rules that determine how data is transmitted between different devices in the same network**_).
- Each layer server the one above it.

#### **1.3 ISO/OSI**

- **ISO** (International Organization for Standardization) published theoretical model for network systems communication, **OSI** (Open System Interconnection).
- **ISO/OSI Layer** : _ALL PEOPLE SEEMS TO NEED DATA PROCESSING_.
	- **Application**
	- **Presentation**
	- **Session**
	- **Transport**
	- **Network**
	- **Data**
	- **Physical**

##### **1.3.1 Layers In Depth**
- TCP/IP Model consist of 4 layer i.e. 1. APPLICATION, 2. TRANSPORT, 3. NETWORK, 4. NETWORK INTERFACE.
- Though 4 Layers are for simplification, actually there are **_5 Layers_ (1. Application, 2. Transport, 3. Network, 4. Data Link, 5. Physical)**

![TCP/IP Model](Images/4_TCPIPModel1.png)

##### **1.3.2 Encapsulation**

- Q) If every protocol has a header and payload, how can a protocol use the one on its lower layer.
- Sol) **_The entire upper protocol packet (header + payload) is the payload of the lower one; this is called [encapsulation](https://study-ccna.com/encapsulation/)._**
- **Each layer adds its own header to  the data supplied by the higher layer.**

![Encapsulation](Images/2_encapsulation2.png)

- **During encapsulation every protocol adds its own header to the packet, treating it as a payload.** This happens to every packet sent to a host.

![Encapsulation](Images/3_encapsulation3.png)

- The receiving host does the same operation in reverse order.

---------------------------------------------------------------------------------------------
<a name="IP"/>

## 2. Internet Protocol.

- We are studying IP Because:
	- Understanding Network Attacks.
	- Using network attacks at there maximum.
	- studying other network protocols.
- IP is in charge of delivering **datagrams** (_IP packets are called datagram_) to the host involved in a communication, and it uses IP addresses to identify a host.
